var http = require('http');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var users = require('./routes/user');
var WebSocketServer = require('ws').Server;
var redis = require("redis");
var app = express();
var port = process.env.PORT || 5000;

app.set('port', port);

var server = http.createServer(app);
server.listen(port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.engine('html', require('ejs').renderFile);

// app.use(favicon(__dirname + '/public/img/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            title: 'error'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
    });
});

var wss = new WebSocketServer({server: server});
console.log("websocket server created");

wss.broadcast = function(data) {
    for(var i in this.clients) {
        this.clients[i].send(data);
    }
};

wss.publish = function(channel, data) {
    var msg = '{ "channel": "' + channel + '", "data": ' + data + '}';
    console.log(msg);
    for(var i in this.clients) {
        this.clients[i].send(msg);
    }
};

wss.on("connection", function(ws) {
    ws.on('message', function(message) {
        console.log('publishing chat message');
        wss.publish("chat", message);
    });

    console.log("websocket connection open");

    ws.on("close", function() {
        console.log("websocket connection close");
    });
});

/* Redis code */

var redis_client = null;

if (process.env.REDISTOGO_URL) {
    var rtg = require("url").parse(process.env.REDISTOGO_URL);
    redis_client = redis.createClient(rtg.port, rtg.hostname);
    redis_client.auth(rtg.auth.split(":")[1]);
} else {
    console.log('using default redis URL');
    redis_client = redis.createClient();
}

if (process.env.TRIA_SERVER_URL) {
    app.locals.base_server_url = process.env.TRIA_SERVER_URL;
    app.set('server_url', process.env.TRIA_SERVER_URL);
} else {
    app.locals.base_server_url = 'http://localhost:8000';
    app.set('server_url', 'http://localhost:8000');
}

if (redis_client) {
    redis_client.on('error', function (err) {
        console.log('Error ' + err);

    });

    redis_client.on("subscribe", function (channel, count) {
        console.log('subscribed to ' + channel);
    });

    redis_client.subscribe('recommendations');

    redis_client.on('message', function (channel, message) {
        /* console.log("client1 channel " + channel + ": " + message); */
        wss.publish("recommendation", message);
    });
}

module.exports = app;
