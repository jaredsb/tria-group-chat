INSTALL
=======

npm install
export PATH=node_modules/.bin:$PATH
bower install

cd public/components/underscore
npm install uglify-js
npm run-script build

cd ../backbone
npm install uglify-js
npm run-script build

cd ../jquery
npm install uglify-js
npm run-script build


Install redis
-------------

On Mac OS X with homebrew:
brew install redis
