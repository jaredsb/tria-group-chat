App.Models.Share = Backbone.RelationalModel.extend({
    urlRoot: App.config.base_server_url + '/shared',
    /* based on code from backbone.js: http://backbonejs.org/docs/backbone.html */
    url: function() {
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        if (this.isNew()) return base + '/';
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/';
    },

    relations: [
        {
            type: Backbone.HasOne,
            key: 'user',
            relatedModel: 'App.Models.UserProfile',
            collectionType: 'App.Collections.UserProfiles',
            reverseRelation: {
                key: 'shared',
                includeInJSON: 'id'
            }
        }
    ]
});

App.Collections.Share = Backbone.Collection.extend({
    url: App.config.base_server_url + '/shared/',
    model: App.Models.Vote,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    },

    share: function(id, shared) {
        App.vent.trigger('shared', id, shared);

    }
});


App.Models.Remove = Backbone.RelationalModel.extend({
    urlRoot: App.config.base_server_url + '/deleted',
    /* based on code from backbone.js: http://backbonejs.org/docs/backbone.html */
    url: function() {
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        if (this.isNew()) return base + '/';
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/';
    },

    relations: [
        {
            type: Backbone.HasOne,
            key: 'user',
            relatedModel: 'App.Models.UserProfile',
            collectionType: 'App.Collections.UserProfiles',
            reverseRelation: {
                key: 'deleted',
                includeInJSON: 'id'
            }
        }
    ]
});

App.Collections.Share = Backbone.Collection.extend({
    url: App.config.base_server_url + '/deleted/',
    model: App.Models.Vote,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    },

    vote: function(id, deleted) {
        App.vent.trigger('deleted', id, deleted);

    }
});
