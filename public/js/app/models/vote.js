App.Models.Vote = Backbone.RelationalModel.extend({
    urlRoot: App.config.base_server_url + '/votes',
    /* based on code from backbone.js: http://backbonejs.org/docs/backbone.html */
    url: function() {
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        if (this.isNew()) return base + '/';
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/';
    },

    relations: [
        {
            type: Backbone.HasOne,
            key: 'user',
            relatedModel: 'App.Models.UserProfile',
            collectionType: 'App.Collections.UserProfiles',
            reverseRelation: {
                key: 'votes',
                includeInJSON: 'id'
            }
        }
    ]
});

App.Collections.Votes = Backbone.Collection.extend({
    url: App.config.base_server_url + '/votes/',
    model: App.Models.Vote,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    },

    vote: function(id, vote) {
        App.vent.trigger('vote', id, vote);

    }
});
