App.Models.GroupPhrase = Backbone.RelationalModel.extend({
    url: function() {
        console.log ( " in groupPhrase url")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        if (this.isNew()) return base;
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/';
    },
    vote_up_url: function() {
         console.log ( " in groupPhrase vote_up_url")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/vote_up';
    },
    vote_up_url: function() {
         console.log ( " in groupPhrase vote_up_url")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/vote_down';
    },

    vote_up: function(model) {
        var url = this.url() + 'vote_up/',
        // note that these are just $.ajax() options
        user_name = App.account.get('user_name')
        options = {

            url: url,
            type: 'POST',
            dataType: 'json',
            data: user_name,
        };

        // add any additional options, e.g. a "success" callback or data
        _.extend(options);

        return (this.sync || Backbone.sync).call(this, null, this, options);
    },

    vote_down: function(model) {
   
        var url = this.url() + 'vote_down/',
        // note that these are just $.ajax() options
        user_name = App.account.get('user_name')
        options = {
            url: url,
            type: 'POST',
            dataType: 'json',
            data: user_name, 
           
        };

        // add any additional options, e.g. a "success" callback or data
        _.extend(options);

        return (this.sync || Backbone.sync).call(this, null, this, options);
    },


   vote: function(account, voteValue, id, model) {
   
        if ( account ){
        var wid = $('#groupphrasespage').width();
        var user_name = account.get('name');
            if (this.has('votes')  &&  voteValue > 0 ) {

            $('[data-id=' + id + ']').css('background', '#64e7d6');
           
            
            $('[data-id=' + id + ']').animate({
                left: wid
            },{
            duration:1000, 
            complete: function() {
               $('[data-id=' + id + ']').remove();
               model.for_user(user_name)
              }
            });

          this.vote_up(model);
          }else if (account.has('name') &&  voteValue < 0 ) {
            var user_name = account.get('name');
            $('[data-id=' + id + ']').css('background', '#fc808c');
            
            $('[data-id=' + id + ']').animate({
                right: wid
            },{
            duration:1000, 
            complete: function() {
               $('[data-id=' + id + ']').remove();
                model.for_user(user_name)
              }
            });
          this.vote_down(model);
        
        }
      }         
            
    }
    
});

App.Collections.GroupPhrases = Backbone.Collection.extend({
    url: function() {
        return App.config.base_server_url + '/groupphrases/';
    },
    model: App.Models.GroupPhrase,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    },
    for_user: function(user_name) {
      this.fetch({ data: { 'user_name': user_name} });
      
    }
});
