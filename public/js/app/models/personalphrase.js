App.Models.PersonalPhrase = Backbone.RelationalModel.extend({
    url: function() {
        console.log(" in  personalphrase.js url LOOK NOW:")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        if (this.isNew()) return base;
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/';
    },
    share_phrase_url: function() {
        console.log(" in  personalphrase.js share_phrase_url:")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/share_phrase';
    },
    delete_phrase_url: function() {
        console.log(" in  personalphrase.js delete_phrase_url:")
        var base =
            _.result(this, 'urlRoot') ||
            _.result(this.collection, 'url') ||
            urlError();
        return base.replace(/([^\/])$/, '$1/') + encodeURIComponent(this.id) + '/delete_phrase';
    },

    share_phrase: function(opts) {

        var url = this.url() + 'share/',
        // note that these are just $.ajax() options
        options = {
            url: url,
            type: 'POST'
        };

        // add any additional options, e.g. a "success" callback or data
        _.extend(options, opts);

        return (this.sync || Backbone.sync).call(this, null, this, options);
    },

    delete_phrase: function(opts) {


        var url = this.url() + 'delete/',
        // note that these are just $.ajax() options
        options = {
            url: url,
            type: 'POST'
        };

        // add any additional options, e.g. a "success" callback or data
        _.extend(options, opts);

        return (this.sync || Backbone.sync).call(this, null, this, options);
    },

    share: function(account, share, id, model) {
   
        if ( account ){
        var wid = $('#personalphrasespage').width();
          if (account.has('name') && share) {
            var user_name = account.get('name');

            $('[data-id=' + id + ']').css('background', '#c4eeff');
           
            
            $('[data-id=' + id + ']').animate({
                left: wid
            },{
            duration:1000, 
            complete: function() {
               $('[data-id=' + id + ']').remove();
               model.for_user(App.account.get('user_name'));
              }
            });

            this.share_phrase();
          }else if (account.has('name') && !share) {
            var user_name = account.get('name');
            $('[data-id=' + id + ']').css('background', '#FF992A');
           
            
            $('[data-id=' + id + ']').animate({
                right: wid
            },{
            duration:1000, 
            complete: function() {
               $('[data-id=' + id + ']').remove();
               model.for_user(App.account.get('user_name'));
              }
            });
            this.delete_phrase();
        
        }
      }         
            
    }
});

App.Collections.PersonalPhrases = Backbone.Collection.extend({
    
    url: function() {
       return App.config.base_server_url + '/personalphrases/';
    },
    model: App.Models.PersonalPhrase,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    },
    for_user: function(user_name) {
      console.log ( this );
      this.fetch({ data: { 'user_name': user_name} });

    }
});
