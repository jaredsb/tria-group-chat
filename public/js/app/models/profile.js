App.Models.UserProfile = Backbone.RelationalModel.extend({
    url: App.config.base_server_url + '/user_profiles/'
});

App.Collections.UserProfiles = Backbone.Collection.extend({
    url: App.config.base_server_url + '/user_profiles/',
    model: App.Models.UserProfile,

    parse: function(response) {
        if (response.hasOwnProperty('results')) {
            return response.results;
        } else {
            return response;
        }
    }
});
