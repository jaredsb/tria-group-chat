App.Models.Message = Backbone.Model.extend({
    url: function() {
        return App.config.base_server_url + '/messages/';
    },

    build: function(msg) {
        if (typeof(msg.from) != 'undefined') {
            var from = new App.Models.Account(msg.from);
            console.log ( msg + " MESSAGE MESSAGE MESSAGE")
            this.set('from', from);
            this.set('source', from.get('user_name'));
        }
    }
});
