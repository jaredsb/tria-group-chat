var PersonalPhraseView = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    className: 'personalphrases-personalphrase',
    template: _.template("<a href=\"#share\" data-rel=\"popup\" data-position-to=\"window\"><%-title%><p style=\"display:inline\"> ▪ <%-description%></p><br/><p id='long-description'><%-long_description%></p></a>"),

    

    events: {
        'click a.ui-btn': 'clickedItem',
        'click a.share-phrase': 'sharePhrase',
        'click a.delete-phrase': 'deletePhrase'
    },

    clickedItem: function(event) {
      
        $( "div.question" ).remove();
        $( "#share" ).prepend( "<div class ='question'>Share <b>" + event.currentTarget.childNodes[0].nodeValue + "</b> with the group?</div>" );
        $("#share").popup("option", "share-id", this.model.id);
    },

    onRender: function() {
        this.$el.attr('data-id', this.model.id);
    }
});

/*
  className: 'message-list',
  childView: GroupPhraseView
 */
App.Views.PersonalPhraseListView = Backbone.Marionette.CollectionView.extend({
     el: '#personalphrases-personalphrases',
     tagName: 'ul',
     itemViewContainer: "ul",
     childView: PersonalPhraseView,

    
    initialize: function() {

         this.collection.bind('add', this.render);
         this.collection.bind('reset', this.render);
         this.collection.bind('sort', this.render);
       
    },

     render: function() {
         this.$el.remove("li");
         Backbone.Marionette.CollectionView.prototype.render.call(this);
         /* console.log(this.collection.length); */
         this.$el.listview('refresh');
     }
 });
