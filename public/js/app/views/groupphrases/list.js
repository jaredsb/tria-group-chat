var GroupPhraseView = Backbone.Marionette.ItemView.extend({
    tagName: 'li',
    className: 'groupphrases-groupphrase',
    
    template: _.template("<a href=\"#vote\" data-rel=\"popup\" data-position-to=\"window\" data-transition=\"pop\"><div><img id=\"img_hold\" src=\"<%-cached_image_url%>\" class=\"ui-li-icon\"/></img><span id=\"image_text\"><%-title%><p style=\"display:inline\"> ▪ <%-description%></p><br></span></div></a>"),

    events: {
        'click a.ui-btn': 'clickedItem',
        'click a.vote-up': 'voteUp',
        'click a.vote-down': 'voteDown'   
    },


    clickedItem: function(event) {
        
        var voteString = event.currentTarget.innerText
        var voteString = voteString.split("▪");
        
        $( "div.questionVote" ).remove();
        $( "#vote" ).prepend( "<div class ='questionVote'>Vote for <b>" + voteString[0] + "</b></div>" );
        $("#vote").popup("option", "vote-id", this.model.id);
    },

    onRender: function() {
        this.$el.attr('data-id', this.model.id);
    }
});

App.Views.GroupPhraseListView = Backbone.Marionette.CollectionView.extend({
    el: '#groupphrases-groupphrases',
    tagName: 'ul',
    itemViewContainer: "ul",
    childView: GroupPhraseView,

    initialize: function() {

        this.collection.bind('add', this.render);
        this.collection.bind('reset', this.render);
        this.collection.bind('sort', this.render);
    },

    render: function() {
        this.$el.remove("li");
        Backbone.Marionette.CollectionView.prototype.render.call(this);
        this.$el.listview('refresh');
    }

});
