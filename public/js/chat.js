App.chat = {};

/*
 * TODO: Restructure this app using Backbone
 */
$(document).on("pageinit", "#mainpage", function(event) {
    if (!App.account) {
        App.flash = 'Please login first';
        App.lastPage = 'chat';
        $.mobile.pageContainer.pagecontainer("change", "profile");
    }

    $("#domcheck").on("click", function() {
        var check = $("#chatpage").length === 1;
        $("#result").text(check);
    });


});

$(document).on("pageinit", "#profilepage", function(event) {
    if (App.flash) {
        $('#flash').html(App.flash);
        App.flash = null;
    }

    function saveName() {
        var check = $('#profile-name').length === 1;
        console.log('in chat.js saveName()');
        App.account =
            new App.Models.Account({
                'name': $('#profile-name').val(),
                'user_name': $('#profile-name').val()
            });

        if (App.lastPage) {
            $.mobile.pageContainer.pagecontainer('change', App.lastPage);
            App.lastPage = null;
        } else {
            $.mobile.pageContainer.pagecontainer('change', 'chat');
            App.lastPage = null;
        }
    }
    /* set focus on the profile name field on pageinit */
    $("input:text:visible:first").focus();

    $("#profile-name").keyup(function (e) {
        if (e.keyCode == 13) {
            saveName();
        }
    });

    $("#save-name").on("click", function() {
        console.log('#save-name click');
        saveName();
    });
});

/*
 * TODO: Build the HTML with a template language
 */
App.chat.onmessage = function(message)
{
    console.log ( message )

    var cl = '';

    if (message.has('source')) {
        if (message.get('source') == App.account.get('user_name')) {
            cl = 'triangle-isosceles left';
        } else {
            cl = 'triangle-isosceles right';
        }
    }

    if (message.has('message')) {
        $li = $('<li class="message">');
        $d = $('<div class="' + cl + '">');
        $li.append($d);

        if (message.has('from')) {
            var from = message.get('from');
            if (from.has('name')) {
                $d.append('<span>' + from.get('name') + ': </span');
            }
        }

        $d.append(message.get('message'));

        $('#chat-messages').append($li);
        $('#chat-messages').listview('refresh');
        $('#chat-messages').scrollTop($('#chat-messages')[0].scrollHeight);
    }
};

App.chat.recvMessage = function(data)
{
    var channel = data.channel;
    var msg = data.data;
    var action = data.data.action;

    if (channel == "chat") {
        var message = new App.Models.Message(msg);
        message.build(msg);

        App.chat.onmessage(message);
    } else if (channel == "groupphrase") {
        if (action == "add") {
            msg = data.data.message;
            var groupphrase = new App.Models.GroupPhrase(msg);
            if (App.hasOwnProperty('groupphrases')) {
                console.log(groupphrase);
                App.groupphrases.add(groupphrase);
            }
        } else if (action == "delete") {

        }
    }
};

App.chat.sendMessage = function(message)
{
    App.ws.send(JSON.stringify(message.toJSON()));
};

$(document).on("pageinit", "#groupphrasespage", function(event) {
    console.log('in chat.js on groupphrasespage');
    /* Setup the listview */
    var height = $('#groupphrasespage').height() - $('#header').height();
    $('#groupphrases-groupphrases').height(height);

    App.groupphrases = new App.Collections.GroupPhrases();

    var groupphrasesView = new App.Views.GroupPhraseListView({
        collection: App.groupphrases
    });

    /* set up an event for voting */
    App.vent.on('vote', function(id, vote) {

        console.log(App.groupphrases);
        var gp = App.groupphrases.get(id);
        console.log("**")
        console.log ( App.groupphrases.models )
        if (typeof(gp) != "undefined") {
            if (App.hasOwnProperty("account") && App.account) {
                gp.vote(App.account, vote, id, App.groupphrases);
            }
        }
    });

    //App.groupphrases.fetch();
    App.groupphrases.for_user(App.account.get('user_name'));
    
    $('#vote a.vote-up').on( { click: function(event) {
     
        var id = $('#vote').popup('option', 'vote-id');
        App.vent.trigger('vote', id, 1);

    }});

    $('#vote a.vote-down').on( { click: function(event) {
     
        var id = $('#vote').popup('option', 'vote-id');
        App.vent.trigger('vote', id, -1);
    
    }});

});


//new personalphrases view

$(document).on("pageinit", "#personalphrasespage", function(event) {
    console.log ( "in chat.js personalphrasespage")
    /* Setup the listview */
    var height = $('#personalphrasespage').height() - $('#header').height();
    $('#personalphrases-personalphrases').height(height);

    App.personalphrases = new App.Collections.PersonalPhrases();

    var personalphrasesView = new App.Views.PersonalPhraseListView({
        collection: App.personalphrases
    });

    /* set up an event for sharing */
    App.vent.on('share', function(id, share) {
        var pp = App.personalphrases.get(id);
        if (typeof(pp) != "undefined") {
            if (App.hasOwnProperty("account") && App.account) {
                pp.share(App.account, share, id, App.personalphrases);
            }
        }
    });

    App.personalphrases.for_user(App.account.get('user_name'));

   
    $('#share a.share-phrase').on( { click: function(event) {
        var id = $('#share').popup('option', 'share-id');
        App.vent.trigger('share', id, true);
    }});

    $('#share a.delete-phrase').on( { click: function(event) {
        var id = $('#share').popup('option', 'share-id');
        //this is for testing and needs to be changed back to true
        App.vent.trigger('share', id, false);
    }});

});


$(document).on("pageinit", "#chatpage", function(event) {
    /* TODO: Refactor this into global page load */
    /* If the user hasn't signed in yet, make them sign in */
    if (!App.account) {
        App.flash = 'Please login first';
        App.lastPage = 'chat';
        $.mobile.pageContainer.pagecontainer("change", "profile");
    }

    /* Setup the listview */
    var height = $('#chatpage').height() - $('#header').height() -
        $('#chat-input').height();
    $('#chat-messages').height(height);
    /* $(document).height() - 500); */
    $('#chat-messages-outer').height(height);

    function sendMessage() {
        var check = $("#chat-message").length === 1;
        var message =
            new App.Models.Message({
                'provider': 'groupchat',
                'from': App.account,
                'source': App.account.get('user_name'),
                'message': $("#chat-message").val(),
            });
        App.chat.sendMessage(message);
        $("#chat-message").val('');
        if (App.config.message_mode = 'single') {
            /* send a single message with a wildcard destination address */
            message.set('dest', '*');
            message.save();
        } else {
            /* TODO: send an individual message for each online user in chat */
            console.log('TODO: implement multi-message posting');
        }
        /* App.chat.onmessage(message); */
    }

    $("#chat-message").keyup(function (e) {
        if (e.keyCode == 13) {
            sendMessage();
        }
    });

    $("#chat-send-message").on("click", function() {
        sendMessage();
    });
});
