/*
 * client-side main application code
 */

App = {};
App.Models = {};
App.Collections = {};
App.Views = {};
App.config = {};

TRIA = {};
TRIA.config = {};

$(document).ready(function() {
    App.config.base_server_url = base_server_url;
    var host = location.origin.replace(/^http/, 'ws');
    /* host = 'ws://localhost:5000/'; */
    App.ws = new WebSocket(host);

    App.ws.onmessage = function (event) {
        console.log(event);
        var msg = JSON.parse(event.data);
        App.chat.recvMessage(msg);
    };

    /* Setup for CORS support */
    $.ajaxSetup({
        headers: {
            'X-Requested-With' : 'XMLHttpRequest',
            'Authorization': "Token " + TRIA.config.serverToken
        }
    });

    /* Set up the event aggregator */
    App.vent = new Backbone.Wreqr.EventAggregator();
});
