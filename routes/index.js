var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', { title: 'Express' });
});

router.get('/header', function(req, res) {
    res.render('header', { pagename: 'header' });
});

router.get('/content', function(req, res) {
    res.render('content', { pagename: 'content' });
});

router.get('/chat', function(req, res) {
    res.render('chat', { pagename: 'chat' });
});

router.get('/groupphrases', function(req, res) {
    res.render('groupphrases', { pagename: 'groupphrases' });
});

router.get('/personalphrases', function(req, res) {
    res.render('personalphrases', { pagename: 'personalphrases' });
});

router.get('/profile', function(req, res) {
    res.render('profile', { pagename: 'profile' });
});

module.exports = router;
